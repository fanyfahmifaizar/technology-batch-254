package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.OrderDetail;
import com.example.pos254.pos.models.OrderHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderHeaderRepo extends JpaRepository<OrderHeader, Long> {
    @Query("SELECT MAX(id) AS maxid FROM OrderHeader")
    Long GetMaxOrderHeader();

    @Query("FROM OrderHeader WHERE lower(Reference) LIKE lower(concat('%',?1,'%')) ")
    public List<OrderHeader> searchOrderHeader(String keyword);

}
