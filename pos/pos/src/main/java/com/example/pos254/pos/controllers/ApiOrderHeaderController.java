package com.example.pos254.pos.controllers;


import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderHeader;
import com.example.pos254.pos.models.Product;
import com.example.pos254.pos.repositories.OrderHeaderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderHeaderController {
    @Autowired
    private OrderHeaderRepo orderHeaderRepo;

    @GetMapping("/orderheader")
    public ResponseEntity<List<OrderHeader>> GetAllOrderHeader(){
        try{
            List<OrderHeader> orderHeader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }
    }

    @GetMapping("/orderheader/{id}")
    public ResponseEntity<List<OrderHeader>> GetOrderHeaderById(@PathVariable("id")Long id){
        try {
            Optional<OrderHeader> orderHeader = this.orderHeaderRepo.findById(id);
            if (orderHeader.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(orderHeader,HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderheadergetmaxid")
    public ResponseEntity<Long> GetOrderHeaderByMaxId(){
        try {
            Long oh = this.orderHeaderRepo.GetMaxOrderHeader();
            System.out.println(oh);
            return new ResponseEntity<>(oh, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/orderheader")
    public ResponseEntity<Object> saveOrderHeader(@RequestBody OrderHeader orderHeader){
        String references = "" + System.currentTimeMillis();
        orderHeader.setReference(references);
        OrderHeader orderHeaderData = this.orderHeaderRepo.save(orderHeader);
        if (orderHeaderData.equals(orderHeader)){

            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderheader/{id}")
    public ResponseEntity<Object> UpdateOrderHeader(@RequestBody OrderHeader orderHeader, @PathVariable("id") Long id){
        Optional<OrderHeader> orderHeaderData = this.orderHeaderRepo.findById(id);
        if(orderHeaderData.isPresent()){
            orderHeader.setId(id);
            this.orderHeaderRepo.save(orderHeader);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/orderheadermapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){
        try{
            List<OrderHeader> orderHeader = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<OrderHeader> pageTuts;

            pageTuts = orderHeaderRepo.findAll(pagingSort);
            orderHeader = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("orderHeader" , orderHeader);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/searchorderheader/{keyword}")
    public ResponseEntity<List<OrderHeader>> getOrderHeaderByReferance(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<OrderHeader> orederHeader = this.orderHeaderRepo.searchOrderHeader(keyword);
            return new ResponseEntity<>(orederHeader, HttpStatus.OK);
        }
        else{
            List<OrderHeader> orderHeader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }
    }
}
