package com.example.pos254.pos.models;

import javax.persistence.*;

@Entity
@Table(name="variant")
public class Variant extends CommonEntity {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long Id;

    @Column(name = "variant_code")
    private String VariantCode;

    @Column(name = "variant_Name")
    private String VariantName;

    @ManyToOne
    @JoinColumn(name="category_id", insertable = false,updatable = false)
    public Category Category;

    @Column(name= "category_id")
    private Long CategoryId;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getVariantCode() {
        return VariantCode;
    }

    public void setVariantCode(String variantCode) {
        VariantCode = variantCode;
    }

    public String getVariantName() {
        return VariantName;
    }

    public void setVariantName(String variantName) {
        VariantName = variantName;
    }

    public com.example.pos254.pos.models.Category getCategory() {
        return Category;
    }

    public void setCategory(com.example.pos254.pos.models.Category category) {
        Category = category;
    }

    public Long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Long categoryId) {
        CategoryId = categoryId;
    }
}
