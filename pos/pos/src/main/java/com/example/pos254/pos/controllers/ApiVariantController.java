package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderHeader;
import com.example.pos254.pos.models.Variant;
import com.example.pos254.pos.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {
    @Autowired
    private VariantRepo variantRepo;

    @GetMapping("/variant")
    public ResponseEntity<List<Variant>> GetAllVariant(){
        try{
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/variant")
    public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){
        try{
            variant.setCreatedOn(new Date());
            variant.setCreatedBy("fahmi");
            this.variantRepo.save(variant);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id")Long id){
        try {
            Optional<Variant> variant = this.variantRepo.findById(id);
            if (variant.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(variant,HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception expection){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id){
        Optional<Variant> variantData = this.variantRepo.findById(id);
        if(variantData.isPresent()){
            variant.setId(id);
            variant.setModifiedOn(new Date());
            variant.setModifiedBy("fahmi");
            this.variantRepo.save(variant);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id){
        this.variantRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/variantbycategory/{id}")
    public ResponseEntity<List<Variant>> GetAllVariantByCategory(@PathVariable("id") Long id){
        try{
            List<Variant> variant = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/variantmapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){
        try{
            List<Variant> variant = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Variant> pageTuts;

            pageTuts = variantRepo.findAll(pagingSort);
            variant = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("variant" , variant);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchvariant/{keyword}")
    public ResponseEntity<List<Variant>> getVariantByVariantName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<Variant> variant = this.variantRepo.searchVariant(keyword);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
        else{
            List<Variant> orderHeader = this.variantRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }
    }

}
