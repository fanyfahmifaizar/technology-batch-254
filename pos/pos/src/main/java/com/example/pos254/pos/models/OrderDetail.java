package com.example.pos254.pos.models;

import javax.persistence.*;

@Entity
@Table
public class OrderDetail extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @ManyToOne
    @JoinColumn(name="order_header_id", insertable = false, updatable = false)
    public OrderHeader OrderHeader;

    @Column(name="order_header_id")
    private long OrderHeaderId;

    @ManyToOne
    @JoinColumn(name="product_id", insertable = false, updatable = false)
    public Product Product;

    @Column(name="product_id")
    private long ProductId;

    @Column(name="quantity")
    private int Quantity;

    @Column(name="price")
    private float Price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public com.example.pos254.pos.models.OrderHeader getOrderHeader() {
        return OrderHeader;
    }

    public void setOrderHeader(com.example.pos254.pos.models.OrderHeader orderHeader) {
        OrderHeader = orderHeader;
    }

    public long getOrderHeaderId() {
        return OrderHeaderId;
    }

    public void setOrderHeaderId(long orderHeaderId) {
        OrderHeaderId = orderHeaderId;
    }

    public com.example.pos254.pos.models.Product getProduct() {
        return Product;
    }

    public void setProduct(com.example.pos254.pos.models.Product product) {
        Product = product;
    }

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }
}
