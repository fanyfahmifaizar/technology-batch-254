package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.OrderDetail;
import com.example.pos254.pos.models.OrderHeader;
import com.example.pos254.pos.models.Variant;
import com.example.pos254.pos.repositories.OrderDetailRepo;
import com.example.pos254.pos.repositories.OrderHeaderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetailController {
    @Autowired
    private OrderDetailRepo orderDetailRepo;

    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail(){
        try{
            List<OrderDetail> orderDetail = this.orderDetailRepo.findAll();
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }
    }

    @GetMapping("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetailById(@PathVariable("id") Long id){
        try{
            List<OrderDetail> orderDetail = this.orderDetailRepo.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/orderdetail")
    public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){

       try{
            orderDetail.setCreatedOn(new Date());
            orderDetail.setCreatedBy("fahmi");
            this.orderDetailRepo.save(orderDetail);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderdetail/{id}")
    public ResponseEntity<Object> UpdateOrderDetail(@RequestBody OrderDetail orderDetail, @PathVariable("id") Long id){

        try{
            orderDetail.setId(id);
            this.orderDetailRepo.save(orderDetail);
            orderDetail.setModifiedOn(new Date());
            orderDetail.setModifiedBy("fahmi");
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        catch(Exception e){
            return ResponseEntity.notFound().build();
        }
    }
}
