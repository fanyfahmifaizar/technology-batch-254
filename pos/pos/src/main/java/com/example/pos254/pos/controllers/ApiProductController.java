package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.Product;
import com.example.pos254.pos.models.Variant;
import com.example.pos254.pos.repositories.ProductRepo;
import com.example.pos254.pos.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiProductController {
    @Autowired
    private ProductRepo productRepo;

    @GetMapping("/product")
    public ResponseEntity<List<Product>> GetAllProduct(){
        try{
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/product")
    public ResponseEntity<Object> saveProduct(@RequestBody Product product){

       try{
            product.setCreatedOn(new Date());
            product.setCreatedBy("fahmi");
            this.productRepo.save(product);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<List<Product>> GetProductById(@PathVariable("id")Long id){
        try {
            Optional<Product> product = this.productRepo.findById(id);
            if (product.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(product,HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception expection){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<List<Product>> UpdateProduct(@RequestBody Product product, @PathVariable("id") Long id){
        Optional<Product> productData = this.productRepo.findById(id);
        if(productData.isPresent()){
            product.setId(id);
            product.setModifiedOn(new Date());
            product.setModifiedBy("fahmi");
            this.productRepo.save(product);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<Object> DeleteProduct(@PathVariable("id") Long id){
        this.productRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/productbyvariant/{id}")
    public ResponseEntity<List<Product>> GetAllProductByVariant(@PathVariable("id") Long id){
        try{
            List<Product> product = this.productRepo.FindByVariantId(id);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchproduct/{keyword}")
    public ResponseEntity<List<Product>> getProductByName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<Product> product = this.productRepo.searchProduct(keyword);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        else{
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
    }

    @GetMapping("/productmapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){
        try{
            List<Product> product = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Product> pageTuts;

            pageTuts = productRepo.findAll(pagingSort);
            product = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("product" , product);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
