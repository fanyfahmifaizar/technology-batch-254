package com.example.employee.employee.repositories;

import com.example.employee.employee.models.Position;
import com.example.employee.employee.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Long> {
    @Query("FROM Role WHERE lower(RoleName) LIKE lower(concat('%',?1,'%')) ")
    public List<Role> searchRole(String keyword);
}
