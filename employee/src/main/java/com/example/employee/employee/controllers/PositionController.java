package com.example.employee.employee.controllers;


import com.example.employee.employee.repositories.PositionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/position/")
public class PositionController {
    @Autowired
    private PositionRepo positionRepo;

    @GetMapping(value= "index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("position/index");
        return view;
    }
}
