package com.example.employee.employee.repositories;

import com.example.employee.employee.models.Employee;
import com.example.employee.employee.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PositionRepo extends JpaRepository<Position, Long> {
    @Query("FROM Position WHERE lower(PositionName) LIKE lower(concat('%',?1,'%')) ")
    public List<Position> searchPosition(String keyword);

}
