package com.example.employee.employee.models;

import javax.persistence.*;

@Entity
@Table(name="position")
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long Id;

    @Column(name = "position_code")
    private String PositionCode;

    @Column(name = "position_name")
    private String PositionName;

    @Column(name = "position_description")
    private String PositionDescription;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getPositionCode() {
        return PositionCode;
    }

    public void setPositionCode(String positionCode) {
        PositionCode = positionCode;
    }

    public String getPositionName() {
        return PositionName;
    }

    public void setPositionName(String positionName) {
        PositionName = positionName;
    }

    public String getPositionDescription() {
        return PositionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        PositionDescription = positionDescription;
    }
}
