package com.example.employee.employee.repositories;

import com.example.employee.employee.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("FROM Employee WHERE lower(EmployeeName) LIKE lower(concat('%',?1,'%')) ")
    public List<Employee> searchEmployee(String keyword);
}
