package com.example.employee.employee.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long Id;

    @NotNull
    @Size(min = 1, message = "NIP tidak boleh kosong")
    @Column(name = "employee_nip")
    private String EmployeeNip;

    @Column(name = "employee_name")
    private String EmployeeName;

    @Column(name = "employee_dob")
    private Date EmployeeDob;

    @Column(name = "employee_pob")
    private String EmployeePob;

    @Column(name = "employee_phone")
    private String EmployeePhone;

    @Column(name = "employee_email")
    private String EmployeeEmail;

    @Column(name = "employee_religion")
    private String EmployeeReligion;

    @ManyToOne
    @JoinColumn(name="position_id", insertable = false, updatable=false)
    public Position position;

    @Column(name="position_id")
    private long PositionId;

    @ManyToOne
    @JoinColumn(name="role_id", insertable = false, updatable=false)
    public Role role;

    @Column(name="role_id")
    private long RoleId;

    @Column(name="sisa_cuti")
    private long SisaCuti = 12;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getEmployeeNip() {
        return EmployeeNip;
    }

    public void setEmployeeNip(String employeeNip) {
        EmployeeNip = employeeNip;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public Date getEmployeeDob() {
        return EmployeeDob;
    }

    public void setEmployeeDob(Date employeeDob) {
        EmployeeDob = employeeDob;
    }

    public String getEmployeePob() {
        return EmployeePob;
    }

    public void setEmployeePob(String employeePob) {
        EmployeePob = employeePob;
    }

    public String getEmployeePhone() {
        return EmployeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        EmployeePhone = employeePhone;
    }

    public String getEmployeeEmail() {
        return EmployeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        EmployeeEmail = employeeEmail;
    }

    public String getEmployeeReligion() {
        return EmployeeReligion;
    }

    public void setEmployeeReligion(String employeeReligion) {
        EmployeeReligion = employeeReligion;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public long getPositionId() {
        return PositionId;
    }

    public void setPositionId(long positionId) {
        PositionId = positionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public long getRoleId() {
        return RoleId;
    }

    public void setRoleId(long roleId) {
        RoleId = roleId;
    }

    public long getSisaCuti() {
        return SisaCuti;
    }

    public void setSisaCuti(long sisaCuti) {
        SisaCuti = sisaCuti;
    }
}
