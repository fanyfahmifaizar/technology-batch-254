package com.example.employee.employee.controllers;

import com.example.employee.employee.models.Cuti;
import com.example.employee.employee.models.Employee;
import com.example.employee.employee.repositories.CutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiController {
    @Autowired
    private CutiRepo cutiRepo;

    @GetMapping("/cuti")
    public ResponseEntity<List<Cuti>> GetAllCuti(){
        try {
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti){
        Cuti cutiData = this.cutiRepo.save(cuti);
        try{
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/cuti/{id}")
    public ResponseEntity<Object> UpdateCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id){
        Optional<Cuti> cutiData = this.cutiRepo.findById(id);
        if(cutiData.isPresent()){
            cuti.setId(id);
            this.cutiRepo.save(cuti);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@PathVariable("id") Long id)
    {
        this.cutiRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/searchcuti/{keyword}")
    public ResponseEntity<List<Cuti>> getcutiByName(@PathVariable("keyword") String keyword){
        if (keyword != null){
            List<Cuti> cuti = this.cutiRepo.searchCuti(keyword);
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        else{
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
    }

    @GetMapping("/cutimapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size){
        try{
            List<Cuti> cuti = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Cuti> pageTuts;

            pageTuts = cutiRepo.findAll(pagingSort);
            cuti = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("cuti" , cuti);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id") Long id){
        try{
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if(cuti.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(cuti, HttpStatus.OK);
                return rest;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
