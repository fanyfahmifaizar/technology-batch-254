package com.example.employee.employee.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="cuti")
public class Cuti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    @Column(name="jenis_cuti")
    private String JenisCuti;

    @Column(name="tanggal_mulai")
    private Date TanggalMulai;

    @Column(name="tanggal_selesai")
    private Date TanggalSelesai;

    @Column(name="lama")
    private String Lama;

    @Column(name="alasan")
    private String Alasan;

    @ManyToOne
    @JoinColumn(name="employee_id", insertable = false, updatable=false)
    public Employee employee;

    @Column(name="employee_id")
    private long EmployeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJenisCuti() {
        return JenisCuti;
    }

    public void setJenisCuti(String jenisCuti) {
        JenisCuti = jenisCuti;
    }

    public Date getTanggalMulai() {
        return TanggalMulai;
    }

    public void setTanggalMulai(Date tanggalMulai) {
        TanggalMulai = tanggalMulai;
    }

    public Date getTanggalSelesai() {
        return TanggalSelesai;
    }

    public void setTanggalSelesai(Date tanggalSelesai) {
        TanggalSelesai = tanggalSelesai;
    }

    public String getLama() {
        return Lama;
    }

    public void setLama(String lama) {
        Lama = lama;
    }

    public String getAlasan() {
        return Alasan;
    }

    public void setAlasan(String alasan) {
        Alasan = alasan;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(long employeeId) {
        EmployeeId = employeeId;
    }
}
