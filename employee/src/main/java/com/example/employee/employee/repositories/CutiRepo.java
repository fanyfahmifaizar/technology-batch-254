package com.example.employee.employee.repositories;

import com.example.employee.employee.models.Cuti;
import com.example.employee.employee.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CutiRepo extends JpaRepository<Cuti, Long> {
    @Query("FROM Cuti a Inner Join Employee b on a.EmployeeId = b.id WHERE lower(b.EmployeeName) LIKE lower(concat('%',?1,'%')) ")
    public List<Cuti> searchCuti(String keyword);
}
